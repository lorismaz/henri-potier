//
//  CartViewController.swift
//  HenriPotier
//
//  Created by Loris Mazloum on 9/30/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import UIKit
import Kingfisher

class CartViewController: UIViewController {
    
    let cartViewModel = CartViewModel()
    
    @IBOutlet weak var cartItemsTableView: UITableView!
    
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var buyNowView: UIView!
    
    @IBOutlet weak var subtotalValueLabel: UILabel!
    @IBOutlet weak var promotionValueLabel: UILabel!
    @IBOutlet weak var finalValueLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Removing shadow line between navigation and pricing block
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "unwindToBooks", sender: self)
    }
    
    private func setupView() {
        priceView.isHidden = cartViewModel.priceViewIsHidden
        buyNowView.isHidden = cartViewModel.buyNowViewisHidden
        
        subtotalValueLabel.text = cartViewModel.subtotalString
        promotionValueLabel.text = cartViewModel.promotionString
        finalValueLabel.text = cartViewModel.finalValueString
    }
}

extension CartViewController: UITableViewDelegate, UITableViewDataSource {
    private func setupTable() {
        cartItemsTableView.delegate = self
        cartItemsTableView.dataSource = self
        cartItemsTableView.tableFooterView = UIView(frame: .zero)
        
        cartItemsTableView.estimatedRowHeight = 44
        cartItemsTableView.rowHeight = UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartViewModel.numberOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cartViewModel.cell(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cartViewModel.heightForCell(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension CartViewController: HPAPIClientDelegate {
    // Protocols demonstration
    func didReceiveNewOffers(offers: [Offer]) {
        cartItemsTableView.reloadData()
    }
}
