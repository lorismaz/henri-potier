//
//  CartViewModel.swift
//  HenriPotier
//
//  Created by Loris Mazloum on 10/5/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import UIKit

class CartViewModel {
    var sessionCart: CartManager = CartManager.cart
    
    //MARK: - UIView ViewModel
    var priceViewIsHidden: Bool {
        return sessionCart.isEmpty
    }
    
    var buyNowViewisHidden: Bool {
        return sessionCart.isEmpty
    }
    
    var subtotalString: String {
        return sessionCart.subtotal.asCurrency
    }
    
    var promotionString: String {
        return "- \(sessionCart.discountValue.asCurrency)"
    }
    
    var finalValueString: String {
        return sessionCart.finalTotal.asCurrency
    }
    
    
    //MARK: - TableView ViewModel
    var numberOfRowsInSection: Int {
        return sessionCart.isEmpty ? 1 : sessionCart.items.count
    }
    
    func cell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        
        if sessionCart.isEmpty {
            return tableView.dequeueReusableCell(withIdentifier: "emptyCartCell")!
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cartCell", for: indexPath) as? CartCell else { fatalError("Can't get a cart cell")}
        let row = indexPath.row
        let currentBook = sessionCart.items[row]
        
        cell.itemTitleLabel.text = currentBook.title
        cell.itemCoverImageView.kf.setImage(with: currentBook.cover, placeholder: UIImage(named: "scar"))
        
        return cell
        
    }
    
    func heightForCell(tableView: UITableView, indexPath: IndexPath) -> CGFloat {
        if sessionCart.isEmpty {
            return tableView.frame.size.height
        } else {
            return 86
        }
    }
}
