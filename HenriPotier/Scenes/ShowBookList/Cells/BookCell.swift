//
//  BookCell.swift
//  HenriPotier
//
//  Created by Loris Mazloum on 9/30/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import UIKit

class BookCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var synopsisTextView: UITextView!
    
    var onAddButtonPressed: ( (BookCell) -> () )?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func addToCartButtonTapped(_ sender: UIButton) {
        self.onAddButtonPressed?(self)
    }
    

}
