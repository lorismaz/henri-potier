//
//  BookListViewController.swift
//  HenriPotier
//
//  Created by Loris Mazloum on 9/28/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import UIKit
import Kingfisher

class BookListViewController: UIViewController {
    //Mark: - Parameters
    let booksViewModel = BookListViewModel()
    
    //Mark: - Outlets
    @IBOutlet weak var bookListTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    func setupView() {
        booksViewModel.delegate = self
        bookListTableView.dataSource = self
        bookListTableView.delegate = self
        bookListTableView.tableFooterView = UIView(frame: .zero)
        
        bookListTableView.estimatedRowHeight = 379.0
        bookListTableView.rowHeight = UITableView.automaticDimension
    }
    
    @IBAction func unwindToBooks(segue: UIStoryboardSegue) {}
    
}

extension BookListViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return booksViewModel.numberOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return booksViewModel.cell(tableView, indexPath: indexPath)
    }
    
}

extension BookListViewController: BookListViewModelDelegate {
    func didReceiveData() {
        self.bookListTableView.reloadData()
    }
    
    func didSendAlert(alert: UIAlertController) {
        self.present(alert, animated: true)
    }
    
    
}
