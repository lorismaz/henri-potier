//
//  BookListViewModel.swift
//  HenriPotier
//
//  Created by Loris Mazloum on 10/5/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import UIKit
import Kingfisher

protocol BookListViewModelDelegate {
    func didReceiveData()
    func didSendAlert(alert: UIAlertController)
}

class BookListViewModel {
    var bookList: [Book] = []
    var sessionCart: CartManager = CartManager.cart
    var delegate: BookListViewModelDelegate?
    
    init() {
        loadBooks()
    }
    
    func loadBooks() {
        HPAPIManager.api.getBooks(completion: { (books) in
            self.bookList = books
            self.delegate?.didReceiveData()
        }) { (error) in
            print("Error: \(error.localizedDescription)")
        }
    }
    
    var numberOfRowsInSection: Int {
        return bookList.count
    }
    
    func cell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "bookCell", for: indexPath) as? BookCell else { fatalError("Can't get a book cell")}
        let row = indexPath.row
        let currentBook = bookList[row]
        
        cell.titleLabel.text = currentBook.title
        
        // Add to cart button
        cell.addToCartButton.setTitle("Add to cart (\(currentBook.price.asCurrency))", for: .normal)
        
        // image
        cell.coverImageView.kf.setImage(with: currentBook.cover, placeholder: UIImage(named: "scar"))
        
        //synopsis
        var synopsisList = ""
        
        for text in currentBook.synopsis {
            synopsisList += text + "\n"
        }
        
        cell.synopsisTextView.text = synopsisList.truncate(length: 100)
        
        //cell action
        cell.onAddButtonPressed = { book in
            guard let cellIndexPath = tableView.indexPath(for: cell) else { return }
            
            let item = self.bookList[cellIndexPath.row]
            
            self.addToCart(book: item, completion: {
                let bookAddedAlert = UIAlertController.createSimpleAlert("Cart", message: "\(currentBook.title) was successfully added to cart.")
                self.delegate?.didSendAlert(alert: bookAddedAlert)
            }, failure: { error in
                let bookNotAddedAlert = UIAlertController.createSimpleAlert("Cart", message: "This book is already in the cart. Purchasing is limited to one title per order.")
                self.delegate?.didSendAlert(alert: bookNotAddedAlert)
            })
        }
        
        return cell
    }
    
    func addToCart(book: Book, completion: @escaping () -> (), failure: @escaping (_ error: Error) -> ()) {
        do {
            try sessionCart.add(book)
            completion()
        } catch CartError.duplicateBook {
            print("Book is already added dude")
            failure(CartError.duplicateBook)
        } catch {
            print("Unknown error while adding a book")
        }
        
    }
    
}
