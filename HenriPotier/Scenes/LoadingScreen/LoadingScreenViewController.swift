//
//  LoadingScreenViewController.swift
//  HenriPotier
//
//  Created by Loris Mazloum on 9/28/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import UIKit

class LoadingScreenViewController: UIViewController {

    //MARK: Properties and Outlets
    @IBOutlet weak var scarLogoImageView: UIImageView!
    
    //MARK: App Life Cycle    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animateLogo()
    }
    
    func animateLogo() {
        UIView.animate(withDuration: 1.5, animations: {
            self.scarLogoImageView.alpha = 1.0
        }) { (complete) in
            self.showBookList()
        }
    }
    
    func showBookList() {
        self.performSegue(withIdentifier: "showBookList", sender: self)
    }

}
