//
//  APIManager.swift
//  HenriPotier
//
//  Created by Loris Mazloum on 9/30/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import Foundation
import Alamofire

protocol HPAPIClientDelegate {
    func didReceiveNewOffers(offers: [Offer])
}

extension HPAPIClientDelegate {
    //Optional methods
    func didReceiveNewOffers(offers: [Offer]) {
    }
}

enum HPAPIServices {
    case books
    case offers(isbns: String)
}

extension HPAPIServices {
    public var ressource : URL {
        switch self {
        case .books:
            guard let url = URL(string: HPAPIManager.baseURL + "books") else { fatalError("incorrect URL string") }
            return url
        case .offers(let isbns):
            guard let url = URL(string: HPAPIManager.baseURL + "books/\(isbns)/commercialOffers") else { fatalError("incorrect URL string") }
            return url
        }
    }
}

class HPAPIManager {
    
    static let api = HPAPIManager()
    
    //MARK: API services
    static let baseURL        = "http://henri-potier.xebia.fr/"
    
    var delegate: HPAPIClientDelegate?
    
    private init() {
    }
    
    func getBooks(completion: @escaping ([Book]) -> (), failure: @escaping (Error) -> ()) {
        
        Alamofire.request(HPAPIServices.books.ressource).responseJSON { (response) in
            
            var bookList: [Book]
            
            switch response.result {
            case .success:
                guard let jsonData = response.data else { return }
                do {
                    bookList = try JSONDecoder().decode([Book].self, from: jsonData)
                    completion(bookList)
                } catch {
                    failure(error)
                }
            case .failure(let error):
                failure(error)
            }
        }
    }
    
    func getImage(from url: String) -> UIImage? {
        guard let imageUrl = URL(string: url) else { return nil  }
        
        do {
            let imageData = try Data(contentsOf: imageUrl)
            guard let image = UIImage(data: imageData) else { return nil }
            return image
        } catch {
            print("Could not create an image out of your link")
        }
        
        return nil
    }
    
    
    func getOffers(for books: [Book], completion: @escaping ([Offer]) -> (), failure: @escaping (Error) -> ()) {
        let isbnString = books.map{$0.isbn}.joined(separator: ",")
        
        Alamofire.request(HPAPIServices.offers(isbns: isbnString).ressource).responseJSON { (response) in
            
            switch response.result {
            case .success:
                guard let jsonData = response.data else { return }
    
                do {
                    let offerList = try JSONDecoder().decode(OfferDictionnary.self, from: jsonData)
                    completion(offerList.offers)
                    if let existingDelegate = self.delegate {
                        existingDelegate.didReceiveNewOffers(offers: offerList.offers)
                    }
                } catch {
                    failure(error)
                }
                
            case .failure(let error):
                failure(error)
            }
        }
    }
    
}


