//
//  CartManager.swift
//  HenriPotier
//
//  Created by Loris Mazloum on 9/30/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import Foundation

enum CartError: Error {
    case duplicateBook
}

extension CartError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .duplicateBook:
            return NSLocalizedString("This book is already in the cart", comment: "message when book is already in cart")
        }
    }
}

class CartManager {
    
    static let cart = CartManager()
    
    private init() {
    }
    
    var items: [Book] = [] {
        didSet {
            getOffers()
        }
    }
    
    var subtotal: Double {
        return items.reduce(0) { (result, book) -> Double in
            result + book.price
        }
    }
    
    var finalTotal: Double {
        return self.subtotal - getBestDiscount()
    }
    
    var discountValue: Double {
        return getBestDiscount()
    }
    
    public var offers: [Offer] = []
    
    private func getOffers() {
        HPAPIManager.api.getOffers(for: self.items, completion: { currentOffers in
            self.offers = currentOffers
        }) { error in
            print(error.localizedDescription)
        }
    }
    
    private func getBestDiscount() -> Double {
        var bestDiscount: Double = 0
        
        for currentOffer in offers {
            let currentDiscount = currentOffer.actualDiscount(for: self.subtotal)
            if currentDiscount > bestDiscount {
                bestDiscount = currentDiscount
            }
        }
        
        return bestDiscount
    }
    
    func add(_ book: Book) throws {
        if self.items.contains(where: { (existingBook) -> Bool in
            existingBook.title == book.title
        }) {
            throw CartError.duplicateBook
        } else {
            self.items.append(book)
        }
        
    }
    
    var isEmpty:Bool {
        return self.items.isEmpty
    }
}
