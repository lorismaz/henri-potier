//
//  Book.swift
//  HenriPotier
//
//  Created by Loris Mazloum on 9/30/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import Foundation

class Book: Decodable {
    var isbn: String
    var title: String
    var price: Double
    var cover: URL
    var synopsis: [String]
}
