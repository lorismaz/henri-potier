//
//  Offer.swift
//  HenriPotier
//
//  Created by Loris Mazloum on 10/2/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import Foundation

class OfferDictionnary: Decodable {
    let offers: [Offer]
    
    init(offers: [Offer]) {
        self.offers = offers
    }
}

class Offer: Decodable {
    let type: String
    let value: Double
    let sliceValue: Double?
    
    public init(type: String, value: Double, sliceValue: Double?) {
        self.type = type
        self.value = value
        self.sliceValue = sliceValue
    }
    
    func actualDiscount(for total: Double) -> Double {
        switch self.type {
        case "minus":
            let discountedPrice = total - self.value
            return discountedPrice > 0 ? self.value : total
        case "percentage":
            return total * self.value / 100
        case "slice":
            guard let sliceValue = self.sliceValue else { return total }
            return Double(Int(total) / Int(sliceValue)) * self.value
        default:
            return total
        }
    }
    
}



