//
//  String+Truncate.swift
//  HenriPotier
//
//  Created by Loris Mazloum on 10/2/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import Foundation

// From https://gist.github.com/viccalexander/0224ab078f76a3af6d79986369d5240b
extension String {
    /**
     Truncates the string to the specified length number of characters and appends an optional trailing string if longer.
     
     - Parameter length: A `String`.
     - Parameter trailing: A `String` that will be appended after the truncation.
     
     - Returns: A `String` object.
     */
    public func truncate(length: Int, trailing: String = "…") -> String {
        if self.count > length {
            return String(self.prefix(length)) + trailing
        } else {
            return self
        }
    }
}
