//
//  Currency+Double.swift
//  HenriPotier
//
//  Created by Loris Mazloum on 10/2/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import Foundation

extension Double {
    public var asCurrency:String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        guard let result = formatter.string(from: self as NSNumber) else { return "" }
        return result
    }
}
