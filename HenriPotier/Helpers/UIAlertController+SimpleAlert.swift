//
//  UIAlertController+SimpleAlert.swift
//  HenriPotier
//
//  Created by Loris Mazloum on 10/2/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import UIKit

public extension UIAlertController {
    
    public class func createSimpleAlert(_ title : String, message : String) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        
        return alert
    }
}
