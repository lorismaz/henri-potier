import UIKit

class Book: Decodable {
    var isbn: String
    var title: String
    var price: Double
    var synopsis: [String]
    
    init(isbn: String, title: String, price: Double, synopsis: [String]) {
        self.isbn = isbn
        self.title = title
        self.price = price
        self.synopsis = synopsis
    }
}

enum OfferType: Decodable {
    init(from decoder: Decoder) throws {
        try self.init(from: decoder)
    }
    
    case percentage
    case minus
    case slice(sliceValue: Double)
}

class Offer: Decodable {
    var type: OfferType
    var value: Double
    
    init(type: OfferType, value: Double) {
        self.type = type
        self.value = value
    }
    
    func actualDiscount(for total: Double) -> Double {
        switch self.type {
        case .minus:
            let discountedPrice = total - self.value
            return discountedPrice > 0 ? self.value : total
        case .percentage:
            return total * self.value / 100
        case .slice(let sliceValue):
            return Double(Int(total) / Int(sliceValue)) * self.value
        }
    }
    
}



let offer1 = Offer(type: .percentage, value: 5)
let offer2 = Offer(type: .minus, value: 15)
let offer3 = Offer(type: .slice(sliceValue: 100), value: 12)

let offerList = [offer1, offer2, offer3]

class Cart {
    var items: [Book] = []
    
    var subtotal: Double {
        return items.reduce(0) { (result, book) -> Double in
            result + book.price
        }
    }
    
    var finalTotal: Double {
        return self.subtotal - getBestDiscount()
    }
    
    public var offers: [Offer] {
        return getOffers()
    }
    
    init(items: [Book]) {
        self.items = items
    }
    
    private func getOffers() -> [Offer] {
        //get offers from API call
        var offers: [Offer] = []
        offers = offerList
        return offers
    }
    
    func getBestDiscount() -> Double {
        var bestDiscount: Double = 0
        
        for currentOffer in offers {
            let currentDiscount = currentOffer.actualDiscount(for: self.subtotal)
            if currentDiscount > bestDiscount {
                bestDiscount = currentDiscount
            }
        }
        
        return bestDiscount
    }
}



let book1 = Book(isbn: "12", title: "Le sang mele", price: 30, synopsis: ["hello"])
let book2 = Book(isbn: "34", title: "la pierre magique", price: 35, synopsis: ["goodbye"])

let cart = Cart(items: [book1, book2])

let isbnString = cart.items.map{$0.isbn}.joined(separator: ",")

let sub = cart.subtotal

let final = cart.finalTotal















